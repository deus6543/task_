-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 01 2020 г., 02:28
-- Версия сервера: 10.0.38-MariaDB
-- Версия PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `task`
--

-- --------------------------------------------------------

--
-- Структура таблицы `data_task`
--

CREATE TABLE IF NOT EXISTS `data_task` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `text` text NOT NULL,
  `status` text NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `data_task`
--

INSERT INTO `data_task` (`id`, `name`, `email`, `text`, `status`, `insert_date`, `edit`) VALUES
(1, 'asd123sas', 'asd', 'asd', 'asd', '2020-05-31 13:57:36', ''),
(2, 'asd', 'asd', 'asd', 'asd', '2020-05-31 13:57:38', ''),
(3, 'asd3', '13', '123', 'asd', '2020-05-31 14:41:07', ''),
(4, 'asd213', 'ads123', 'asd123', 'asd', '2020-05-31 14:41:07', ''),
(5, 'dsa', '1', 'dsa', 'done', '2020-05-31 14:41:26', ''),
(6, 'asd321', 'asd321', 'asd123', 'ads', '2020-05-31 15:19:11', ''),
(7, 'asd123', 'asd123', 'asd321', 'ads', '2020-05-31 15:19:32', ''),
(8, 'asd12323', 'asd', 'asd', 'ads', '2020-05-31 15:19:32', ''),
(9, 'asd123', 'asd', 'asd', 'ads', '2020-05-31 15:19:32', ''),
(10, 'asd23', 'asd23', 'asd1234', 'ads', '2020-05-31 15:19:32', ''),
(11, 'asd23', 'asd', 'asd', 'ads', '2020-05-31 15:19:32', ''),
(12, 'asd', 'asd', 'asd', 'ads', '2020-05-31 15:19:32', ''),
(13, 'asd', 'asd', 'asd', 'ads', '2020-05-31 15:19:32', ''),
(14, 'asd', 'asd', 'asd', 'ads', '2020-05-31 15:19:32', ''),
(15, 'asd', 'asd', 'asd', 'ads', '2020-05-31 15:19:32', ''),
(16, 'asd', 'asd', 'asd', 'asd', '2020-05-31 15:20:00', ''),
(17, 'asd', 'asd', 'asd', 'asd', '2020-05-31 15:20:00', ''),
(18, 'asd', 'asd', 'asd', 'asd', '2020-05-31 15:20:59', ''),
(19, 'asd', 'asd', 'asd', 'asd', '2020-05-31 15:20:59', ''),
(20, 'asd', 'asd', 'asd', 'not_done', '2020-05-31 15:20:59', ''),
(21, 'asd', 'asd', 'asd', 'asd', '2020-05-31 15:20:59', ''),
(22, 'asd', 'asd', 'asd', 'asd', '2020-05-31 15:20:59', ''),
(23, 'asd', 'asd', 'asd', 'not_done', '2020-05-31 15:20:59', 'admin'),
(24, 'asd', 'asd', 'asd', 'asd', '2020-05-31 15:20:59', ''),
(25, 'sad', 'asd@sdf.ru', '0', '', '2020-05-31 18:33:02', ''),
(26, 'sad', 'asd@sdf.ru', 'sDA', '', '2020-05-31 18:34:17', ''),
(27, 'sada', 'sadfs@rt.ru', 'asdsadasd', '', '2020-05-31 18:34:30', ''),
(28, 'qwer', 'wer@rt.ru', 'asdasdasdasdasd', '', '2020-05-31 18:35:21', ''),
(29, '123', '21321@ru.ru', '213', '', '2020-05-31 21:36:18', ''),
(30, 'asd321', 'ss@22.ru', '\r\nalert("123");\r\n', '', '2020-05-31 21:55:51', ''),
(31, 'sad', 'test@test.ru', '123', '', '2020-05-31 21:59:44', ''),
(32, 'asaa', 'deus6543@gmail.com', 'ололоas6345', 'not_done', '2020-05-31 22:25:18', 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL,
  `login` text NOT NULL,
  `password` text NOT NULL,
  `role` text NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `login`
--

INSERT INTO `login` (`id`, `login`, `password`, `role`, `insert_time`) VALUES
(1, 'admin', 'ad7NbXmflAESA', 'admin', '2020-05-31 19:53:06');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `data_task`
--
ALTER TABLE `data_task`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `data_task`
--
ALTER TABLE `data_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT для таблицы `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
